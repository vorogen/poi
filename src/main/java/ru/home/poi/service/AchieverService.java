package ru.home.poi.service;

import org.codehaus.plexus.archiver.tar.TarArchiver;
import org.codehaus.plexus.archiver.zip.ZipArchiver;
import ru.home.poi.util.NullLogger;

import java.io.IOException;
import java.nio.file.Path;

public class AchieverService {
    public void gzipCompress(Path inFilePath, Path outFilePath) {
        try {
            TarArchiver.TarCompressionMethod compression = new TarArchiver.TarCompressionMethod();
            compression.setValue("gzip");

            TarArchiver tar = new TarArchiver();
            tar.enableLogging(NullLogger.INSTANCE);
            tar.setCompression(compression);
            tar.setDestFile(outFilePath.toFile());
            tar.addDirectory(inFilePath.toFile());
            tar.createArchive();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void zipCompress(Path inFilePath, Path outFilePath) {
        try {
            ZipArchiver zipArchiver = new ZipArchiver();
            zipArchiver.enableLogging(NullLogger.INSTANCE);
            zipArchiver.addDirectory(inFilePath.toFile());
            zipArchiver.setDestFile(outFilePath.toFile());
            zipArchiver.createArchive();
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }
}
