package ru.home.poi.service;

import java.nio.file.Files;
import java.nio.file.Path;

public class Md5generatorService {
    public String getMd5(Path filePath) {
        try {
            byte[] data = Files.readAllBytes(filePath);

            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(data);
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
