package ru.home.poi.service;

import ru.home.poi.bean.DirectorySize;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class SizeService {
    public DirectorySize getDirectorySize(Path dirPath) {
        try {
            List<Path> filePaths = Files.find(dirPath, Integer.MAX_VALUE, (p, a) -> !p.toFile().isDirectory()).collect(Collectors.toList());

            DirectorySize result = new DirectorySize();
            result.setSize(calcFilesLength(filePaths, 0));
            result.setSize4(calcFilesLength(filePaths, 4));
            result.setSize8(calcFilesLength(filePaths, 8));
            result.setSize16(calcFilesLength(filePaths, 16));
            result.setSize32(calcFilesLength(filePaths, 32));

            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private long calcFilesLength(List<Path> filePaths, int size) {
        BigDecimal clusterSize = BigDecimal.valueOf(size == 0 ? 1 : size * 1024);
        return filePaths.stream()
                .mapToLong(p -> BigDecimal.valueOf(p.toFile().length())
                        .divide(clusterSize, 0, RoundingMode.UP)
                        .multiply(clusterSize)
                        .longValue())
                .sum();
    }
}
