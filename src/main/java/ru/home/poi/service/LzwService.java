package ru.home.poi.service;

import ru.home.poi.OperationException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LzwService {
    public Long readPointCount(File pointFile) throws OperationException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        readPoints(pointFile, os);

        long result = os.toString().split("km/h", -1).length;
        if (result == 0) {
            throw new OperationException("No one point found in file " + pointFile.toPath());
        }
        return result - 1;
    }

    private void readPoints(File pointFile, OutputStream os) throws OperationException {
        LzwTable lzwTable = new LzwTable();

        try {
            byte[] data = Files.readAllBytes(pointFile.toPath());
            try (InputStream is = new ByteArrayInputStream(data)) {
                readUnsignedWord(is);
                int nelem = readUnsignedDWord(is);
                List<Tab> tabList = readTabs(is, nelem);

                for (int i = 0; i < nelem; i++) {
                    int size = tabList.get(i + 1).dataOffset - tabList.get(i).dataOffset;
                    int dataOffset = tabList.get(i).dataOffset;
                    byte[] subData = Arrays.copyOfRange(data, dataOffset, dataOffset + size);

                    if (tabList.get(i).unOffset != readLzwSector(os, lzwTable, subData, size)) {
                        throw new OperationException("LZW decompress error for file: " + pointFile.toPath());
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void addStringToLzwTable(LzwTable lzwTable, int oldCode, LzwString lzwString) {
        lzwTable.lzwStringBuffer[lzwTable.lzwNtab++] = lzwStringConcat(lzwTable.lzwStringBuffer[oldCode], lzwString.value.charAt(0));
        if (lzwTable.lzwNtab == (1 << lzwTable.lzwBitsCode)) {
            lzwTable.lzwBitsCode++;
        }
    }

    private LzwString lzwStringConcat(LzwString src, char symbol) {
        LzwString result = new LzwString();
        result.length = src.length + 1;
        result.value = src.value + symbol;
        return result;
    }

    private int getNextLzwCode(LzwTable lzwTable, byte[] data, int size) {
        while (lzwTable.lzwBitsLast < lzwTable.lzwBitsCode) {
            if (lzwTable.lzwDptr >= size) {
                return Integer.MIN_VALUE;
            }
            byte symbol = data[lzwTable.lzwDptr++];

            lzwTable.lzwCodeLast <<= 8;
            lzwTable.lzwCodeLast |= Byte.toUnsignedInt(symbol);
            lzwTable.lzwBitsLast += 8;
        }

        int code = lzwTable.lzwCodeLast;
        code >>= (lzwTable.lzwBitsLast - lzwTable.lzwBitsCode);
        code &= (1 << lzwTable.lzwBitsCode) - 1;
        lzwTable.lzwBitsLast -= lzwTable.lzwBitsCode;
        return code;
    }

    private void writeLzwString(OutputStream os, LzwTable lzwTable, int code) throws IOException {
        for (int i = 0; i < lzwTable.lzwStringBuffer[code].length; i++) {
            os.write(new byte[]{(byte) lzwTable.lzwStringBuffer[code].value.charAt(i)});
        }
        lzwTable.lzwOutSize += lzwTable.lzwStringBuffer[code].length;
    }

    private boolean isLzwCodeInTable(LzwTable lzwTable, int code) {
        return code < lzwTable.lzwNtab;
    }

    private long readLzwSector(OutputStream os, LzwTable lzwTable, byte[] data, int size) throws IOException {
        int code = Integer.MIN_VALUE;
        int oldcode = Integer.MIN_VALUE;
        lzwTable.lzwDptr = 0;

        do {
            if (code == Integer.MIN_VALUE || lzwTable.lzwDptr % 929 == 0) {
                initLzwTable(lzwTable);
                code = getNextLzwCode(lzwTable, data, size);
                if (code != Integer.MIN_VALUE) {
                    writeLzwString(os, lzwTable, code);
                    oldcode = code;
                }
            } else if (isLzwCodeInTable(lzwTable, code)) {
                addStringToLzwTable(lzwTable, oldcode, lzwTable.lzwStringBuffer[code]);
                writeLzwString(os, lzwTable, code);
                oldcode = code;
            } else {
                addStringToLzwTable(lzwTable, oldcode, lzwTable.lzwStringBuffer[oldcode]);
                writeLzwString(os, lzwTable, lzwTable.lzwNtab - 1);
                oldcode = code;
            }

            code = getNextLzwCode(lzwTable, data, size);
        } while (code != 0x101 && lzwTable.lzwDptr < size);

        return lzwTable.lzwOutSize;
    }

    private void initLzwTable(LzwTable lzwTable) {
        for (int i = 0; i < 256; i++) {
            LzwString dst = new LzwString();
            dst.length = 1;
            dst.value = String.valueOf((char) i);
            lzwTable.lzwStringBuffer[i] = dst;
        }

        lzwTable.lzwNtab = 258;
        lzwTable.lzwBitsCode = 9;
        lzwTable.lzwBitsLast = 0;
        lzwTable.lzwCodeLast = 0;
    }

    private int readByte(InputStream is) throws IOException, OperationException {
        int a1 = is.read();
        if (a1 == -1) {
            throw new OperationException("Unexpected end of file");
        }
        return a1;
    }

    private int readUnsignedWord(InputStream is) throws IOException, OperationException {
        int a1 = readByte(is);
        int a2 = readByte(is);
        return a2 << 8 | a1;
    }

    private int readUnsignedDWord(InputStream is) throws IOException, OperationException {
        int a1 = readUnsignedWord(is);
        int a2 = readUnsignedWord(is);
        return (a2 << 16 | a1);
    }

    private List<Tab> readTabs(InputStream is, int nelem) throws IOException, OperationException {
        int tabOffset = readUnsignedDWord(is);
        readUnsignedDWord(is);

        for (int i = 0; i < tabOffset - 14; i++) {
            readByte(is);
        }

        List<Tab> tabList = new ArrayList<>();
        for (int i = 0; i < nelem; i++) {
            tabList.add(new Tab(readUnsignedDWord(is), readUnsignedDWord(is)));
        }

        int lastTabIndex = nelem - 1;
        tabList.get(lastTabIndex).unOffset = tabList.get(lastTabIndex).unOffset + 1;
        tabList.add(new Tab(tabOffset, Integer.MIN_VALUE));

        return tabList;
    }

    private static class LzwString {
        String value;
        int length;
    }

    private static class Tab {
        int dataOffset;
        int unOffset;

        Tab(int dataOffset, int unOffset) {
            this.unOffset = unOffset;
            this.dataOffset = dataOffset;
        }
    }

    private static class LzwTable {
        int lzwDptr = 0;
        int lzwOutSize = 0;
        int lzwNtab = 258;
        int lzwBitsCode = 9;
        int lzwBitsLast = 0;
        int lzwCodeLast = 0;

        LzwString[] lzwStringBuffer = new LzwString[4096];
    }
}

