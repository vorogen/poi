package ru.home.poi.service;

import ru.home.poi.OperationException;
import ru.home.poi.util.PoiFileUtil;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;

public class PoiConverterService {
    private PoiService poiService = new PoiService();
    private AchieverService achieverService = new AchieverService();
    private PoiArchiveService poiArchiveService = new PoiArchiveService();
    private ResultAchieverService resultAchieverService = new ResultAchieverService();

    public Path convert(Path workingDir, List<Path> poiPaths) throws OperationException {
        var workingPoiDir = PoiFileUtil.createDir(workingDir.resolve("TMP_POI"));
        poiService.processPoiFiles(poiPaths, workingPoiDir);

        var poiArchivePath = workingDir.resolve("ZAR_POI.BIN");
        achieverService.gzipCompress(workingDir, poiArchivePath);

        var poiArchiveMetadataPath = workingPoiDir.resolve("ZAR_POI.BIN.inf");
        poiArchiveService.createPoiArchiveMetadata(poiArchiveMetadataPath, poiPaths, poiArchivePath);

        var resultDir = PoiFileUtil.createDir(workingDir.resolve("result"));
        resultAchieverService.createResultArchive(resultDir, poiArchivePath, poiArchiveMetadataPath);

        Path resultArchive = workingDir.resolve("POI_" + LocalDate.now() + ".zip");
        achieverService.zipCompress(resultDir, resultArchive);
        return resultArchive;
    }
}
