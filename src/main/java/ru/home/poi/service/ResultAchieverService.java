package ru.home.poi.service;

import ru.home.poi.util.PoiFileUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class ResultAchieverService {
    private Md5generatorService md5generatorService = new Md5generatorService();

    public void createResultArchive(Path workingDir, Path poiArchivePath, Path poiArchiveMetadataPath) {
        copyTemplateFiles(workingDir);
        fillDataDirectory(workingDir, poiArchivePath, poiArchiveMetadataPath);
        fillMd5File(workingDir, poiArchivePath, poiArchiveMetadataPath);
    }

    private void copyTemplateFiles(Path workingDir) {
        PoiFileUtil.copyFromJar("/template", workingDir);
    }

    private void fillDataDirectory(Path workingDir, Path poiArchivePath, Path poiArchiveMetadataPath) {
        var dataDir = PoiFileUtil.createDir(workingDir.resolve("DATA"));

        var base1Dir = PoiFileUtil.createDir(dataDir.resolve("BASE1"));
        PoiFileUtil.copyFile(poiArchivePath, base1Dir);
        PoiFileUtil.copyFile(poiArchiveMetadataPath, base1Dir);

        var base2Dir = PoiFileUtil.createDir(dataDir.resolve("BASE2"));
        PoiFileUtil.copyFile(poiArchivePath, base2Dir);
        PoiFileUtil.copyFile(poiArchiveMetadataPath, base2Dir);
    }

    private void fillMd5File(Path workingDir, Path poiArchivePath, Path poiArchiveMetadataPath) {
        try {
            String archiveMd5 = md5generatorService.getMd5(poiArchivePath);
            String metadataMd5 = md5generatorService.getMd5(poiArchiveMetadataPath);

            Path filePath = workingDir.resolve("MEDIA_CONTENT.md5");
            String data = getMd5FileData(filePath, archiveMd5, metadataMd5);

            Files.write(filePath, data.getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getMd5FileData(Path filePath, String archiveMd5, String metadataMd5) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader fr = new BufferedReader(new FileReader(filePath.toFile()))) {
            String line;
            while ((line = fr.readLine()) != null) {
                if (line.endsWith("BASE1\\ZAR_POI.BIN")) {
                    sb.append(archiveMd5).append(" *DATA\\BASE1\\ZAR_POI.BIN\r\n");
                } else if (line.endsWith("BASE1\\ZAR_POI.BIN.inf")) {
                    sb.append(metadataMd5).append(" *DATA\\BASE1\\ZAR_POI.BIN.inf\r\n");
                } else if (line.endsWith("BASE2\\ZAR_POI.BIN")) {
                    sb.append(archiveMd5).append(" *DATA\\BASE2\\ZAR_POI.BIN\r\n");
                } else if (line.endsWith("BASE2\\ZAR_POI.BIN.inf")) {
                    sb.append(metadataMd5).append(" *DATA\\BASE2\\ZAR_POI.BIN.inf\r\n");
                } else {
                    sb.append(line).append("\r\n");
                }
            }
        }
        return sb.toString();
    }
}
