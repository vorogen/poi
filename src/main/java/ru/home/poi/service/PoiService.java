package ru.home.poi.service;

import ru.home.poi.OperationException;
import ru.home.poi.bean.DirectorySize;
import ru.home.poi.bean.PoiMetadata;
import ru.home.poi.bean.PoiType;
import ru.home.poi.parser.PoiMetadataParser;
import ru.home.poi.util.PoiFileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class PoiService {
    private PoiMetadataParser poiMetadataParser = new PoiMetadataParser();
    private LzwService lzwService = new LzwService();
    private SizeService sizeService = new SizeService();

    public void processPoiFiles(List<Path> paths, Path workingPoiDir) throws OperationException {
        for (Path path : paths) {
            processPoiFile(workingPoiDir, path);
        }
    }

    private void processPoiFile(Path workingDir, Path poiPath) throws OperationException {
        var poiDir = PoiFileUtil.createDir(workingDir.resolve(poiPath.getFileName()));
        for (PoiType poiType : PoiType.values()) {
            String tmpPoiDirName = poiPath.toFile().getName() + "_" + poiType.name();
            Path tmpPoiDir = PoiFileUtil.createDir(workingDir.resolve(tmpPoiDirName));
            writePoiFiles(poiType, tmpPoiDir, poiPath);

            PoiFileUtil.copyFiles(tmpPoiDir, poiDir);
            PoiFileUtil.deleteDirectory(tmpPoiDir);
        }
    }

    private void writePoiFiles(PoiType poiType, Path workingDir, Path poiPath) throws OperationException {
        File metadataFile = PoiFileUtil.getMetadataFile(poiPath, poiType);
        File pointFile = PoiFileUtil.getPointFile(poiPath, poiType);
        if (metadataFile != null && pointFile != null) {
            PoiMetadata poiMetadata = poiMetadataParser.parse(poiType, metadataFile);
            poiMetadata.setNumberPoi(lzwService.readPointCount(pointFile));

            copyPoiFiles(poiMetadata.getPoiType(), workingDir, poiPath);

            for (int i = 0; i < 2; i++) {
                enrichPoiMetadata(workingDir, poiMetadata);
                createMetadataFile(workingDir, poiMetadata.getPoiType(), poiMetadata);
            }
        }
    }

    private void enrichPoiMetadata(Path tmpDir, PoiMetadata poiMetadata) {
        DirectorySize directorySize = sizeService.getDirectorySize(tmpDir);
        poiMetadata.setPoiSize32(directorySize.getSize32());
        poiMetadata.setPoiSize16(directorySize.getSize16());
        poiMetadata.setPoiSize8(directorySize.getSize8());
        poiMetadata.setPoiSize4(directorySize.getSize4());
        poiMetadata.setPoiSize(directorySize.getSize());
    }

    private void copyPoiFiles(PoiType poiType, Path tmpDir, Path poiPath) {
        try {
            File[] files = PoiFileUtil.getPoiFiles(poiType, poiPath);
            for (File file : files) {
                Files.copy(file.toPath(), tmpDir.resolve(file.getName()));
            }
        } catch (IOException e) {
            throw new RuntimeException("Can't copy poi files", e);
        }
    }

    private void createMetadataFile(Path tpmDirPath, PoiType poiType, PoiMetadata poiMetadata) {
        try {
            File metadataFile = PoiFileUtil.getMetadataFile(tpmDirPath, poiType);
            String fileData = poiMetadataParser.parse(poiMetadata);
            Files.write(metadataFile.toPath(), fileData.getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Can't create metadata file", e);
        }
    }
}
