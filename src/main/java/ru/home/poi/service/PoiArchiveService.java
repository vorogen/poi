package ru.home.poi.service;

import ru.home.poi.OperationException;
import ru.home.poi.bean.DirectorySize;
import ru.home.poi.bean.Pair;
import ru.home.poi.bean.PoiArchiveMetadata;
import ru.home.poi.parser.PoiArchiveMetadataParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;

public class PoiArchiveService {
    private PoiArchiveMetadataParser poiArchiveMetadataParser = new PoiArchiveMetadataParser();
    private SizeService sizeService = new SizeService();
    private CrcService crcService = new CrcService();

    public void createPoiArchiveMetadata(Path poiArchiveMetadataPath, List<Path> poiPaths, Path poiArchivePath) throws OperationException {
        var continentPair = PoiArchiveMetadataParser.resolveContinent(poiPaths);

        PoiArchiveMetadata metadata = createPoiArchiveMetadata(continentPair);
        enrichAndWritePoiArchiveMetadata(poiArchiveMetadataPath, metadata);

        addCrcToPoiArchiveMetadata(metadata, poiArchivePath, poiArchiveMetadataPath);
    }

    private void addCrcToPoiArchiveMetadata(PoiArchiveMetadata metadata, Path poiArchivePath, Path poiArchiveMetadataPath) {
        String checksum = crcService.getCrc(poiArchivePath);
        metadata.setChecksum(checksum);
        createArchiveMetadataFile(poiArchiveMetadataPath, metadata);

        checksum = crcService.getCrc(poiArchiveMetadataPath);
        metadata.setChecksum(checksum + metadata.getChecksum());
        createArchiveMetadataFile(poiArchiveMetadataPath, metadata);
    }

    private void enrichAndWritePoiArchiveMetadata(Path poiArchiveMetadataPath, PoiArchiveMetadata metadata) {
        for (int i = 0; i < 3; i++) {
            DirectorySize directorySize = sizeService.getDirectorySize(poiArchiveMetadataPath.getParent());
            metadata.setPoiSize32(directorySize.getSize32());
            metadata.setPoiSize16(directorySize.getSize16());
            metadata.setPoiSize8(directorySize.getSize8());
            metadata.setPoiSize4(directorySize.getSize4());
            metadata.setPoiSize(directorySize.getSize());

            createArchiveMetadataFile(poiArchiveMetadataPath, metadata);
        }
    }

    private void createArchiveMetadataFile(Path poiArchiveMetadataPath, PoiArchiveMetadata metadata) {
        try {
            String fileData = poiArchiveMetadataParser.parse(metadata);
            Files.write(poiArchiveMetadataPath, fileData.getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private PoiArchiveMetadata createPoiArchiveMetadata(Pair<String, String> continentPair) {
        var result = new PoiArchiveMetadata();
        result.setChecksum("11111111");
        result.setContinentId(continentPair.getFirst());
        result.setContinentName(continentPair.getSecond());
        result.setMediaName("POI_GENERATOR");
        result.setPoiProvider("comiri");
        result.setVersion(LocalDate.now());
        return result;
    }

}
