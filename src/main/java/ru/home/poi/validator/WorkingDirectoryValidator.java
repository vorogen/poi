package ru.home.poi.validator;

import ru.home.poi.OperationException;
import ru.home.poi.bean.PoiType;
import ru.home.poi.util.PoiFileUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class WorkingDirectoryValidator {
    public void validateInputDirectories(List<Path> paths) throws OperationException {
        for (var path : paths) {
            availabilityCheck(path);
            validatePoiPath(path);
        }
    }

    private void validatePoiPath(Path path) throws OperationException {
        File dir = path.toFile();
        boolean isDangersPresent = isPresentFiles(dir, PoiType.DANGERZ);
        boolean isRadarsPresent = isPresentFiles(dir, PoiType.RADAR);

        if (!isDangersPresent && !isRadarsPresent) {
            throw new OperationException(dir + " directory no contain radar or dangerous files");
        }

        int filesSize = Optional.ofNullable(dir.listFiles()).map(d -> d.length).orElse(0);
        if (isDangersPresent && isRadarsPresent && filesSize != 10) {
            throw new OperationException(dir + " directory has to contain only radar or dangerous files");
        } else if (isDangersPresent && !isRadarsPresent && filesSize != 5) {
            throw new OperationException(dir + " directory has to contain only dangerous files");
        } else if (isRadarsPresent && !isDangersPresent && filesSize != 5) {
            throw new OperationException(dir + " directory has to contain only radar files");
        }
    }

    private boolean isPresentFiles(File dir, PoiType poiType) throws OperationException {
        var files = PoiFileUtil.getPoiFileNames(poiType, dir.toPath());
        if (files == null || files.length == 0) {
            return false;
        }
        if (files.length != 5) {
            throw new OperationException(dir + " directory should contain 5 files of type: " + poiType);
        }
        var data = Set.of(files);
        checkFilePresent(dir, data, poiType + ".LZW");
        checkFilePresent(dir, data, poiType + "_PE.LZW");
        checkFilePresent(dir, data, poiType + "_PS.LZW");
        checkFilePresent(dir, data, poiType + "DSC.LZW");
        checkFilePresent(dir, data, "POI_VER_" + poiType + ".TXT");
        return true;
    }

    private void checkFilePresent(File dir, Set<String> files, String fileName) throws OperationException {
        if (!files.contains(fileName)) {
            throw new OperationException(dir + " directory does not contain file with name " + fileName);
        }
    }

    public void validateOutputDirectory(Path outputPath) throws OperationException {
        availabilityCheck(outputPath);

    }

    private void availabilityCheck(Path outputPath) throws OperationException {
        if (!Files.exists(outputPath)) {
            throw new OperationException("Directory does not exist by path " + outputPath);
        }

        File dir = outputPath.toFile();
        if (!dir.isDirectory()) {
            throw new OperationException("Directory is expected by path " + outputPath);
        }
    }
}
