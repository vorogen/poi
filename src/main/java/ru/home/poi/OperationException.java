package ru.home.poi;

public class OperationException extends Exception {
    public OperationException(String message) {
        super(message);
    }
}
