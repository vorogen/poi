package ru.home.poi.util;

import org.codehaus.plexus.logging.Logger;

public class NullLogger implements Logger {
    public static final NullLogger INSTANCE = new NullLogger();

    @Override
    public void debug(String s) {

    }

    @Override
    public void debug(String s, Throwable throwable) {

    }

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public void info(String s) {

    }

    @Override
    public void info(String s, Throwable throwable) {

    }

    @Override
    public boolean isInfoEnabled() {
        return false;
    }

    @Override
    public void warn(String s) {

    }

    @Override
    public void warn(String s, Throwable throwable) {

    }

    @Override
    public boolean isWarnEnabled() {
        return false;
    }

    @Override
    public void error(String s) {

    }

    @Override
    public void error(String s, Throwable throwable) {

    }

    @Override
    public boolean isErrorEnabled() {
        return false;
    }

    @Override
    public void fatalError(String s) {

    }

    @Override
    public void fatalError(String s, Throwable throwable) {

    }

    @Override
    public boolean isFatalErrorEnabled() {
        return false;
    }

    @Override
    public Logger getChildLogger(String s) {
        return null;
    }

    @Override
    public int getThreshold() {
        return 0;
    }

    @Override
    public String getName() {
        return null;
    }
}
