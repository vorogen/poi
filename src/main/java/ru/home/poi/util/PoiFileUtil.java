package ru.home.poi.util;

import ru.home.poi.bean.PoiType;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class PoiFileUtil {
    public static File getMetadataFile(Path path, PoiType poiType) {
        String poiFileName = "POI_VER_" + poiType + ".TXT";

        var result = path.toFile().listFiles((s, fileName) -> poiFileName.equals(fileName));
        return result == null || result.length == 0 ? null : result[0];
    }

    public static File getPointFile(Path path, PoiType poiType) {
        var result = path.toFile().listFiles((s, fileName) -> (poiType + ".LZW").equals(fileName));
        return result != null ? result[0] : null;
    }

    public static File[] getPoiFiles(PoiType poiType, Path path) {
        return path.toFile().listFiles((d, fileName) -> fileName.contains(poiType.name()));
    }

    public static String[] getPoiFileNames(PoiType poiType, Path path) {
        return path.toFile().list((d, fileName) -> fileName.contains(poiType.name()));
    }

    public static Path createTempDir() {
        try {
            Path result = Files.createTempDirectory("poi_creator");
            result.toFile().deleteOnExit();
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Can't create temporary directory", e);
        }
    }

    public static Path createDir(Path path) {
        try {
            if (Files.exists(path)) {
                PoiFileUtil.deleteDirectory(path);
            }
            return Files.createDirectory(path);
        } catch (IOException e) {
            throw new RuntimeException("Can't create directory by " + path, e);
        }
    }

    public static void deleteDirectory(Path path) {
        try {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new RuntimeException("Can't delete directory by " + path, e);
        }
    }

    public static void copyFiles(Path fromDir, Path toDir) {
        try {
            File[] files = fromDir.toFile().listFiles();
            if (files != null) {
                for (File file : files) {
                    Files.copy(file.toPath(), toDir.resolve(file.getName()));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Can't copy file to " + toDir, e);
        }
    }

    public static void copyFile(Path file, Path dir) {
        try {
            Files.copy(file, dir.resolve(file.getFileName()));
        } catch (IOException e) {
            throw new RuntimeException("Can't copy file to " + dir, e);
        }
    }

    public static void copyFromJar(String resource, Path toDir) {
        try {
            URI resourceUri = PoiFileUtil.class.getResource(resource).toURI();
            if (!resourceUri.toString().startsWith("file:")) {
                Map<String, String> env = new HashMap<>();
                env.put("create", "true");
                FileSystems.newFileSystem(resourceUri, env);
            }
            Path resourcePath = Paths.get(resourceUri);

            Files.walkFileTree(resourcePath, new HashSet<>(), 2, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path child, BasicFileAttributes attrs) throws IOException {
                    return doCopy(child);
                }

                @Override
                public FileVisitResult preVisitDirectory(Path child, BasicFileAttributes attrs) throws IOException {
                    return doCopy(child);
                }

                private FileVisitResult doCopy(Path child) throws IOException {
                    var fileName = child.toString().replace(resourcePath.toString(), "");

                    String targetPath = toDir.toAbsolutePath() + File.separator + fileName;
                    Files.copy(child, Paths.get(targetPath), StandardCopyOption.REPLACE_EXISTING);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("Can't copy template files to " + toDir, e);
        }
    }
}
