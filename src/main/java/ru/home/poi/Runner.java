package ru.home.poi;


import ru.home.poi.bean.ApplicationParams;
import ru.home.poi.service.PoiConverterService;
import ru.home.poi.util.PoiFileUtil;
import ru.home.poi.validator.WorkingDirectoryValidator;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Runner {
    private WorkingDirectoryValidator workingDirectoryValidator = new WorkingDirectoryValidator();

    private PoiConverterService poiConverterService = new PoiConverterService();

    public static void main(String[] args) {
        ApplicationParams applicationParams = parseApplicationParams(args);

        Runner runner = new Runner();
        runner.perform(applicationParams.getPoiPaths(), applicationParams.getOutDir());
    }

    private static String readArgValue(String[] args, int index) {
        return index < args.length ? args[index] : null;
    }

    private static void printErrorAndExit(String errorMessage) {
        System.err.println(errorMessage);
        System.exit(-1);
    }

    private static ApplicationParams parseApplicationParams(String[] args) {
        ApplicationParams result = new ApplicationParams();
        for (int index = 0; index < args.length; index++) {
            if ("-p".equals(args[index])) {
                String value = readArgValue(args, ++index);
                if (value == null || value.startsWith("-")) {
                    printErrorAndExit("Incorrect set -p param. Use pattern -p <firstFilePath>,<secondFilePath>");
                } else {
                    List<Path> paths = Arrays.stream(value.split(",")).map(s -> Paths.get(s)).collect(Collectors.toList());
                    result.setPoiPaths(paths);
                }
            } else if ("-o".equals(args[index])) {
                String value = readArgValue(args, ++index);
                if (value == null || value.startsWith("-")) {
                    printErrorAndExit("Incorrect set -o param. Use pattern -o <outputPath>");
                } else {
                    result.setOutDir(Paths.get(value));
                }
            }
        }
        if (result.getOutDir() == null || result.getPoiPaths() == null) {
            printErrorAndExit("Mandatory params: \n" +
                    "   -o output path (directory should exist)\n" +
                    "   -p poi directories path (list directories with POI data: -p ./016,./021)");
        }

        return result;
    }

    private void perform(List<Path> paths, Path outputPath) {
        try {
            workingDirectoryValidator.validateInputDirectories(paths);
            workingDirectoryValidator.validateOutputDirectory(outputPath);

            var mainDir = PoiFileUtil.createTempDir();
            Path resultArchive = poiConverterService.convert(mainDir, paths);
            PoiFileUtil.copyFile(resultArchive, outputPath);
        } catch (OperationException e) {
            printErrorAndExit(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            printErrorAndExit("Internal app error");
            System.exit(-1);
        }
    }

}
