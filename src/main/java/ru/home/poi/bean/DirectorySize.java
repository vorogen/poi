package ru.home.poi.bean;

public class DirectorySize {
    private Long size;
    private Long size4;
    private Long size8;
    private Long size16;
    private Long size32;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getSize4() {
        return size4;
    }

    public void setSize4(Long size4) {
        this.size4 = size4;
    }

    public Long getSize8() {
        return size8;
    }

    public void setSize8(Long size8) {
        this.size8 = size8;
    }

    public Long getSize16() {
        return size16;
    }

    public void setSize16(Long size16) {
        this.size16 = size16;
    }

    public Long getSize32() {
        return size32;
    }

    public void setSize32(Long size32) {
        this.size32 = size32;
    }
}
