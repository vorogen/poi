package ru.home.poi.bean;

import java.time.LocalDate;

public class PoiMetadata {
    private PoiType poiType;
    private String poiProvider;
    private String poiMacroCat;
    private String poiCat;
    private LocalDate datePoi;
    private String nameIcon;
    private String nameSound;
    private String description;
    private String prefix;
    private String nameCountry;
    private String cid;
    private Long numberPoi;
    private Long poiSize32;
    private Long poiSize16;
    private Long poiSize8;
    private Long poiSize4;
    private Long poiSize;

    public PoiType getPoiType() {
        return poiType;
    }

    public void setPoiType(PoiType poiType) {
        this.poiType = poiType;
    }

    public String getPoiProvider() {
        return poiProvider;
    }

    public void setPoiProvider(String poiProvider) {
        this.poiProvider = poiProvider;
    }

    public String getPoiMacroCat() {
        return poiMacroCat;
    }

    public void setPoiMacroCat(String poiMacroCat) {
        this.poiMacroCat = poiMacroCat;
    }

    public String getPoiCat() {
        return poiCat;
    }

    public void setPoiCat(String poiCat) {
        this.poiCat = poiCat;
    }

    public LocalDate getDatePoi() {
        return datePoi;
    }

    public void setDatePoi(LocalDate datePoi) {
        this.datePoi = datePoi;
    }

    public String getNameIcon() {
        return nameIcon;
    }

    public void setNameIcon(String nameIcon) {
        this.nameIcon = nameIcon;
    }

    public String getNameSound() {
        return nameSound;
    }

    public void setNameSound(String nameSound) {
        this.nameSound = nameSound;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public Long getNumberPoi() {
        return numberPoi;
    }

    public void setNumberPoi(Long numberPoi) {
        this.numberPoi = numberPoi;
    }

    public Long getPoiSize32() {
        return poiSize32;
    }

    public void setPoiSize32(Long poiSize32) {
        this.poiSize32 = poiSize32;
    }

    public Long getPoiSize16() {
        return poiSize16;
    }

    public void setPoiSize16(Long poiSize16) {
        this.poiSize16 = poiSize16;
    }

    public Long getPoiSize8() {
        return poiSize8;
    }

    public void setPoiSize8(Long poiSize8) {
        this.poiSize8 = poiSize8;
    }

    public Long getPoiSize4() {
        return poiSize4;
    }

    public void setPoiSize4(Long poiSize4) {
        this.poiSize4 = poiSize4;
    }

    public Long getPoiSize() {
        return poiSize;
    }

    public void setPoiSize(Long poiSize) {
        this.poiSize = poiSize;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
}
