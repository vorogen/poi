package ru.home.poi.bean;

public class Pair<F, S> {
    private F first;
    private S second;

    private Pair() {
    }

    public static <F, S> Pair<F, S> of(F first, S second) {
        Pair<F, S> result = new Pair<>();
        result.first = first;
        result.second = second;
        return result;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }
}
