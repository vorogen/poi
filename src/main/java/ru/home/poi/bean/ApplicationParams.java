package ru.home.poi.bean;

import java.nio.file.Path;
import java.util.List;

public class ApplicationParams {
    private List<Path> poiPaths;
    private Path outDir;

    public List<Path> getPoiPaths() {
        return poiPaths;
    }

    public void setPoiPaths(List<Path> poiPaths) {
        this.poiPaths = poiPaths;
    }

    public Path getOutDir() {
        return outDir;
    }

    public void setOutDir(Path outDir) {
        this.outDir = outDir;
    }
}
