package ru.home.poi.bean;

import java.time.LocalDate;

public class PoiArchiveMetadata {
    private String checksum;
    private String continentId;
    private String continentName;
    private String mediaName;
    private String poiProvider;
    private LocalDate version;
    private Long poiSize32;
    private Long poiSize16;
    private Long poiSize8;
    private Long poiSize4;
    private Long poiSize;

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getContinentId() {
        return continentId;
    }

    public void setContinentId(String continentId) {
        this.continentId = continentId;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public String getPoiProvider() {
        return poiProvider;
    }

    public void setPoiProvider(String poiProvider) {
        this.poiProvider = poiProvider;
    }

    public LocalDate getVersion() {
        return version;
    }

    public void setVersion(LocalDate version) {
        this.version = version;
    }

    public Long getPoiSize32() {
        return poiSize32;
    }

    public void setPoiSize32(Long poiSize32) {
        this.poiSize32 = poiSize32;
    }

    public Long getPoiSize16() {
        return poiSize16;
    }

    public void setPoiSize16(Long poiSize16) {
        this.poiSize16 = poiSize16;
    }

    public Long getPoiSize8() {
        return poiSize8;
    }

    public void setPoiSize8(Long poiSize8) {
        this.poiSize8 = poiSize8;
    }

    public Long getPoiSize4() {
        return poiSize4;
    }

    public void setPoiSize4(Long poiSize4) {
        this.poiSize4 = poiSize4;
    }

    public Long getPoiSize() {
        return poiSize;
    }

    public void setPoiSize(Long poiSize) {
        this.poiSize = poiSize;
    }
}
