package ru.home.poi.parser;

import ru.home.poi.OperationException;
import ru.home.poi.bean.PoiMetadata;
import ru.home.poi.bean.PoiType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import static ru.home.poi.parser.ParserUtil.checkOrDefault;

public class PoiMetadataParser {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final DateTimeFormatter reverse_formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    public PoiMetadata parse(PoiType poiType, File metadataFile) throws OperationException {
        try {
            Path filePath = metadataFile.toPath();
            List<String> fileData = Files.readAllLines(filePath);

            PoiMetadata poiMetadata = new PoiMetadata();
            poiMetadata.setPoiType(poiType);
            poiMetadata.setPoiProvider(fetchValue(filePath, fileData, "POI_PROVIDER"));
            poiMetadata.setPoiMacroCat(fetchValue(filePath, fileData, "POI_MACRO_CAT"));
            poiMetadata.setPoiCat(fetchValue(filePath, fileData, "POI_CAT"));
            poiMetadata.setDatePoi(LocalDate.of(2020, 9, 9)); //fetchLocalDateValue(filePath, fileData, "DATA_POI"));
            poiMetadata.setNameIcon(fetchValue(filePath, fileData, "NAME_ICON"));
            poiMetadata.setNameSound(fetchValue(filePath, fileData, "NAME_SOUND"));
            poiMetadata.setDescription(fetchValue(filePath, fileData, "DESCRIPTION"));
            poiMetadata.setPrefix(fetchValue(filePath, fileData, "PREFIX"));
            poiMetadata.setNameCountry(fetchValue(filePath, fileData, "NAME_COUNTRY"));
            poiMetadata.setCid(metadataFile.getParentFile().getName());
            poiMetadata.setNumberPoi(safetyFetchLongValue(filePath, fileData, "NUMBER_POI"));
            poiMetadata.setPoiSize32(safetyFetchLongValue(filePath, fileData, "POI_SIZE_32"));
            poiMetadata.setPoiSize16(safetyFetchLongValue(filePath, fileData, "POI_SIZE_16"));
            poiMetadata.setPoiSize8(safetyFetchLongValue(filePath, fileData, "POI_SIZE_8"));
            poiMetadata.setPoiSize4(safetyFetchLongValue(filePath, fileData, "POI_SIZE_4"));
            poiMetadata.setPoiSize(safetyFetchLongValue(filePath, fileData, "POI_SIZE"));

            return poiMetadata;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String fetchValue(Path filePath, List<String> fileData, String key) throws OperationException {
        String result = safetyFetchValue(fileData, key);
        if (result == null || result.isBlank()) {
            throw new OperationException(key + " key not found in file by " + filePath);
        }
        return result;
    }

    private String safetyFetchValue(List<String> fileData, String key) throws OperationException {
        return fileData.stream()
                .filter(s -> s.startsWith(key))
                .map(s -> s.replaceAll(key, ""))
                .map(s -> s.replaceAll(":", ""))
                .map(s -> s.replaceAll("_", ""))
                .findFirst()
                .orElse(null);
    }

    private Long safetyFetchLongValue(Path filePath, List<String> fileData, String key) throws OperationException {
        try {
            String result = safetyFetchValue(fileData, key);
            return result == null ? null : Long.valueOf(result);
        } catch (NumberFormatException e) {
            throw new OperationException(key + " key contains no long value in file by " + filePath);
        }
    }

    private LocalDate fetchLocalDateValue(Path filePath, List<String> fileData, String key) throws OperationException {
        try {
            return LocalDate.parse(fetchValue(filePath, fileData, key), formatter);
        } catch (DateTimeParseException e) {
            throw new OperationException(key + " key has wrong date format in file by " + filePath);
        }
    }

    public String parse(PoiMetadata poiMetadata) {
        return "POI_PROVIDER:" + checkOrDefault(poiMetadata.getPoiProvider()) + "\r\n" +
                "POI_MACRO_CAT:" + checkOrDefault(poiMetadata.getPoiMacroCat()) + "\r\n" +
                "POI_CAT:" + checkOrDefault(poiMetadata.getPoiCat()) + "\r\n" +
                "DATE:" + reverse_formatter.format(poiMetadata.getDatePoi()) + "\r\n" +
                "DATA_POI:" + formatter.format(poiMetadata.getDatePoi()) + "\r\n" +
                "NAME_ICON:" + checkOrDefault(poiMetadata.getNameIcon()) + "\r\n" +
                "NAME_SOUND:" + checkOrDefault(poiMetadata.getNameSound()) + "\r\n" +
                "DESCRIPTION:" + checkOrDefault(poiMetadata.getDescription()) + "\r\n" +
                "PREFIX:" + checkOrDefault(poiMetadata.getPrefix()) + "\r\n" +
                "NAME_COUNTRY:" + checkOrDefault(poiMetadata.getNameCountry()) + "\r\n" +
                "CID:" + checkOrDefault(poiMetadata.getCid()) + "\r\n" +
                "NUMBER_POI:" + checkOrDefault(poiMetadata.getNumberPoi()) + "\r\n" +
                "POI_SIZE_32:" + checkOrDefault(poiMetadata.getPoiSize32()) + "\r\n" +
                "POI_SIZE_16:" + checkOrDefault(poiMetadata.getPoiSize16()) + "\r\n" +
                "POI_SIZE_8:" + checkOrDefault(poiMetadata.getPoiSize8()) + "\r\n" +
                "POI_SIZE_4:" + checkOrDefault(poiMetadata.getPoiSize4()) + "\r\n" +
                "POI_SIZE:" + checkOrDefault(poiMetadata.getPoiSize());
    }
}
