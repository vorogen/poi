package ru.home.poi.parser;

public class ParserUtil {
    public static String checkOrDefault(String value) {
        return value == null ? "" : value;
    }

    public static String checkOrDefault(Long value) {
        return value == null ? "" : value.toString();
    }
}
