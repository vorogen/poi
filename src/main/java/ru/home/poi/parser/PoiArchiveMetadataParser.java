package ru.home.poi.parser;

import ru.home.poi.OperationException;
import ru.home.poi.bean.Pair;
import ru.home.poi.bean.PoiArchiveMetadata;

import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.home.poi.parser.ParserUtil.checkOrDefault;

public class PoiArchiveMetadataParser {
    private static final String ALL_RUSSIA_CID = "033";
    private static final String PART_RUSSIA_CID = "016";
    private static final String BELARUS_CID = "021";

    private static final DateTimeFormatter reverse_formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    public static Pair<String, String> resolveContinent(List<Path> poiPaths) throws OperationException {
        Set<String> poiCidSet = toCidSet(poiPaths);
        if (poiCidSet.size() == 2 && poiCidSet.contains(PART_RUSSIA_CID) && poiCidSet.contains(BELARUS_CID)) {
            return Pair.of("01", "EUROPE");
        } else if (poiCidSet.size() == 1) {
            if (poiCidSet.contains(PART_RUSSIA_CID) || poiCidSet.contains(BELARUS_CID)) {
                return Pair.of("01", "EUROPE");
            } else if (poiCidSet.contains(ALL_RUSSIA_CID)) {
                return Pair.of("17", "RUSSIA");
            }
        }
        throw new OperationException(poiCidSet + " cid set is not supported");
    }

    private static Set<String> toCidSet(List<Path> poiPaths) {
        return poiPaths.stream()
                .map(p -> p.toFile().getName())
                .collect(Collectors.toSet());

    }

    public String parse(PoiArchiveMetadata metadata) {
        return checkOrDefault(metadata.getChecksum()) + "\r\n" +
                "CONTINENT_ID:" + checkOrDefault(metadata.getContinentId()) + "\r\n" +
                "CONTINENT_NAME:" + checkOrDefault(metadata.getContinentName()) + "\r\n" +
                "MEDIA_NAME:" + checkOrDefault(metadata.getMediaName()) + "\r\n" +
                "POI_PROVIDER:" + checkOrDefault(metadata.getPoiProvider()) + "\r\n" +
                "VERSION:" + reverse_formatter.format(metadata.getVersion()) + "\r\n" +
                "CID_SIZE_32:" + checkOrDefault(metadata.getPoiSize32()) + "\r\n" +
                "CID_SIZE_16:" + checkOrDefault(metadata.getPoiSize16()) + "\r\n" +
                "CID_SIZE_8:" + checkOrDefault(metadata.getPoiSize8()) + "\r\n" +
                "CID_SIZE_4:" + checkOrDefault(metadata.getPoiSize4()) + "\r\n" +
                "CID_SIZE:" + checkOrDefault(metadata.getPoiSize());
    }
}
