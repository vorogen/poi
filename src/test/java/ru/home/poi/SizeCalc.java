package ru.home.poi;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

//tar -czvf file.tar.gz /full/path - создать .tar.gz (архив)
//tar -xvf file.tar.gz  распаковать

//zip -r filename.zip folder
public class SizeCalc {
    //private static final String PATH = "/Users/ruavrg1/IdeaProjects/poi/src/main/resources/orig_archive/";
    //private static final String PATH = "/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp/TMP_POI/033/tmp";
     private static final String PATH = "/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp/";

    public static void main(String[] args) throws IOException {
        new SizeCalc().run();
    }

    private void run() throws IOException {
        Path path = Paths.get(PATH);
        List<Path> filePaths = Files.find(path, 1, (p, a) -> !p.toFile().isDirectory())
                .collect(Collectors.toList());

        System.out.println("POI_SIZE_32:" + getFilesLength(filePaths, 32));
        System.out.println("POI_SIZE_16:" + getFilesLength(filePaths, 16));
        System.out.println("POI_SIZE_8:" + getFilesLength(filePaths, 8));
        System.out.println("POI_SIZE_4:" + getFilesLength(filePaths, 4));
        System.out.println("POI_SIZE:" + getFilesLength(filePaths, 0));
    }

    private long getFilesLength(List<Path> filePaths, int size) {
        BigDecimal clusterSize = BigDecimal.valueOf(size == 0 ? 1 : size * 1024);

        return filePaths.stream()
                .mapToLong(p -> BigDecimal.valueOf(p.toFile().length())
                        .divide(clusterSize, 0, RoundingMode.UP)
                        .multiply(clusterSize)
                        .longValue())
                .sum();
    }
}

/*
POI_SIZE_32:491520
POI_SIZE_16:425984
POI_SIZE_8:393216
POI_SIZE_4:385024
POI_SIZE:373971
*/
