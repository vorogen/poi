package ru.home.poi;

import org.codehaus.plexus.archiver.tar.TarArchiver;
import org.codehaus.plexus.archiver.tar.TarGZipUnArchiver;
import org.codehaus.plexus.logging.console.ConsoleLoggerManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Achiever {
    /*for unpack*/
    private static final Path DATA_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp/ZAR_POI.BIN");
    private static final Path DESTINATION_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp");

    /*for pack*/
    //private static final Path DATA_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp");
    //private static final Path DESTINATION_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/tmp/ZAR_POI.BIN");

    public static void main(String[] args) throws IOException {
        ConsoleLoggerManager manager = new ConsoleLoggerManager();
        manager.initialize();

        File inFile = DATA_PATH.toFile();
        File outFile = DESTINATION_PATH.toFile();

        if (!inFile.exists()) {
            System.err.println("File or directory not found by " + DATA_PATH);
            System.exit(-1);
        }

        if (inFile.isDirectory()) {
            compress(manager, inFile, outFile);
        } else {
            uncompress(manager, inFile, outFile);
        }
    }

    private static void uncompress(ConsoleLoggerManager manager, File inFile, File outFile) {
        TarGZipUnArchiver ua = new TarGZipUnArchiver();
        ua.enableLogging(manager.getLoggerForComponent("u"));

        ua.setSourceFile(inFile);
        ua.setDestDirectory(outFile);
        ua.extract();
    }

    private static void compress(ConsoleLoggerManager manager, File inFile, File outFile) throws IOException {
        TarArchiver.TarCompressionMethod compression = new TarArchiver.TarCompressionMethod();

        compression.setValue("gzip");

        TarArchiver tar = new TarArchiver();
        tar.enableLogging(manager.getLoggerForComponent("u"));
        tar.setCompression(compression);
        tar.setDestFile(outFile);
        tar.addDirectory(inFile);
        tar.createArchive();
    }
}
