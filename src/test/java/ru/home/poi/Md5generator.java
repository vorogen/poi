package ru.home.poi;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Md5generator {
    private static final Path BIN_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/work/DATA/BASE1/ZAR_POI.BIN");
    private static final Path INFO_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/work/DATA/BASE1/ZAR_POI.BIN.inf");
    private static final Path POI_UPGRADE_SUB_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/work/UPG/POI_UPGRADE_SUB.CMD");
    private static final Path POI_UPGRADE_SUB_RNEG_PATH = Paths.get("/Users/ruavrg1/IdeaProjects/POI/src/main/resources/work/UPG/POI_UPGRADE_SUB_RNEG.CMD");

    public static void main(String[] args) throws Exception {
        System.out.println("BIN: " + MD5(new String(Files.readAllBytes(BIN_PATH))));
        System.out.println("INFO: " + MD5(new String(Files.readAllBytes(INFO_PATH))));
        System.out.println("POI_UPGRADE_SUB: " + MD5(new String(Files.readAllBytes(POI_UPGRADE_SUB_PATH))));
        System.out.println("POI_UPGRADE_SUB_RNEG: " + MD5(new String(Files.readAllBytes(POI_UPGRADE_SUB_RNEG_PATH))));
    }

    private static String MD5(String md5) throws Exception {
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        byte[] array = md.digest(md5.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : array) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }
}
