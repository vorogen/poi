This is tool for POI files converter to archive which will be applied by smeg/rt6 automatically

Next POI file structure is supported:
- 016
-- DANGERZ.LZW
-- DANGERZ_PE.LZW
-- DANGERZ_PS.LZW
-- DANGERZDSC.LZW
-- POI_VER_DANGERZ.TXT
-- POI_VER_RADAR.TXT
-- RADAR.LZW
-- RADAR_PE.LZW
-- RADAR_PS.LZW
-- RADARDSC.LZW

for run this tool java > 11 should be installed and next command is applied:
java -jar release/poi-0.1.jar -p tmp/012,tmp/021 -o resultFolder

where:
    -p list of POI directories
    -o output directory for result archive